# GameDevAssets
Browse though a curated list of free assets for your games. Aiming for a clean, simple and easy way to find assets for your game!

## Submitting assets
_You need basic knowledge about [Git](https://git-scm.com), [GitLab](https://gitlab.com), [Jekyll](http://jekyllrb.com) & [Ruby](https://www.ruby-lang.org/en/) to follow the following steps:_

1. Read the [rules](#rules) before submitting anything.
2. Fork the site on [GitLab](https://gitlab.com/GameDevAssets/gamedevassets.gitlab.io/forks/new), and clone your fork to your desktop.
3. Create a new folder in the `/assets` folder, named after your name.
    - For organizing purposes I suggest something like: `/assets/your-name-folder/your-assets-folder`, that way if you submit additional assets in the future you can put them into the folder with your name in it.
4. Drop your assets into the newly created folder.
5. Create a new post in the `/_posts` folder _([naming of the post is important](http://jekyllrb.com/docs/posts/#the-posts-folder))_ and fill out the relevant YAML fields _([see below](#editable-fields--explanations))_.
  - I suggest in the lines of: `YEAR-MONTH-DAY-YourName-YourAssets.md` (all in lowercase)
  - Example: `2015-11-23-saucy-wallpapers.md`
6. Inside the root folder, run cmd `jekyll serve` _(see [Official Jekyll site](http://jekyllrb.com/docs/usage/) for more info)_.
7. If Jekyll is up and running, enter this page: [http://localhost:4000/](http://localhost:4000/)
8. Find your post, test if all links work and all information is displayed correctly. Then push your changes up and open a pull request.

## Rules
- Upload only your own assets, **you must be the author**.
- Assets **must be allowed** to be used in a free or commercial game (authors can leave a license note on the page, telling users how they'd like to be credited).

## Editable fields & explanations
| Field              | Description |
| ------------------ | ------------- |
| title              | The title of your assets. |
| date               | The date of the creation of the post. |
| folder             | The name of your assets folder (your folder inside the /assets folder). |
| homepage           | (optional) The assets homepage (if it has have any). External link. |
| preview            | (optional) A link for the users to preview the assets. Example: it could be a link to an “overview” sprite sheet that shows all available sprites. External or internal link. |
| download           | The download link to your assets. External or internal link. |
| license            | The license for your assets. Choose a License is a great site to review licensing options. |
| license_notes      | (optional) Additional license information. Example: If you’re using the MIT license, but you don’t want people to even provide credit back to you. You can leave that note here. |
| license_url        | (optional) A direct link to the license file or website page. External or internal link. |
| author             | Your name, alias or whatever. |
| social_media       | (optional) Collection of links to the assets author. External links. |
| images/thumbnail   | The preview image for your assets. Image size: 315x250. External or internal link. |
| images/banner      | (optional) Big preview image for your assets. Recommended image size at least: 640x360. External or internal link. |
| images/screenshots | (optional) Screenshots of your assets. External or internal links. |
| tags               | Self explanatory. Example: A sprite sheet might have different animation states in it, list them all in the tags section: Idle, Walk, Run, Attack, etc |

## License
[MIT License](LICENSE.md)
