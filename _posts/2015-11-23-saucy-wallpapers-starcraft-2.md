---
layout:        post
title:         Starcraft 2 wallpapers
date:          2015-11-23 04:33:42
folder:        /saucy/wallpapers/starcraft-2 # Name of your assets folder
homepage:      https://gitlab.com/GameDevAssets/gamedevassets.gitlab.io
preview:       protoss.png
download:      wallpapers.zip
license:       MIT License
license_notes: Do whatever you'd like with the wallpapers.
license_url:   LICENSE.md
author:        Saucy
social_media:  # Full urls (leave blank to disable)
  website:     http://saucy.se
  twitter:     
  facebook:    
  reddit:      # Subreddit
  tumblr:      
  bitbucket:   https://bitbucket.org/Saucy/
  github:      https://github.com/Saucy
  gitlab:      https://gitlab.com/Saucyminator
  deviantart:  http://ninjasaus.deviantart.com
  google-plus:
  vimeo:       
  youtube:     
  soundcloud:  
images:
  thumbnail:   protoss_thumbnail.png # 315x250
  banner:      protoss.png # MIN: 640x360, MAX: any 16:9 ratio image above 640x360 works
  screenshots:
    #- protoss.png
    - zerg.png
    - terran.png
    - protoss_thumbnail.png
    - zerg_thumbnail.png
    - terran_thumbnail.png
tags:
  - Wallpapers
  - 'Starcraft 2: Wings of Liberty'
  - Tech-tree
---

Starcraft 2: Wings of Liberty wallpapers, includes help about the tech-trees and unit counters for all races. [Original thread](http://eu.battle.net/sc2/en/forum/topic/441825881#1) on Battle.net.

Source .psd files are included in the .zip file!

Starcraft 2 [font on dafont.com](http://www.dafont.com/starcraft.font)

_Updated 2015-11-26: Fix both .psd files. Reduced their file size (was over 30mb), layer & groups structure (easier to find the different races)._
