---
layout:        post
title:         'Example: 2D Sprites'
date:          2015-11-27 03:20:37
folder:        /example/2d-sprites # Name of your assets folder
homepage:      http://opengameart.org/content/generic-platformer-tiles
preview:       generic_platformer_mockup.png
download:      generic_platformer_tiles_1.png
license:       CC0
license_notes:
license_url:   http://opengameart.org/content/generic-platformer-tiles
author:        surt
social_media:  # Full urls (leave blank to disable)
  website:     
  twitter:     
  facebook:    
  reddit:      # Subreddit
  tumblr:      
  bitbucket:   
  github:      
  gitlab:      
  deviantart:  
  google-plus:
  vimeo:       
  youtube:     
  soundcloud:  
images:
  thumbnail:   thumbnail.png # 315x250
  banner:      generic_platformer_mockup.png # MIN: 640x360, MAX: any 16:9 ratio image above 640x360 works
  screenshots:
tags:
  - Examples
---

An example how a 2D sprites asset page would look like. _(I personally have zero knowledge on how to make 2D sprites) // Saucy_
