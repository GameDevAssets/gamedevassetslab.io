---
layout:        post
title:         'Example: 3D Model'
date:          2015-11-27 02:44:16
folder:        /example/3d-model # Name of your assets folder
homepage:      http://opengameart.org/content/low-spec-airspace-fighter
preview:       fighter.png
download:      fighter.blend
license:       CC0
license_notes:
license_url:   http://opengameart.org/content/low-spec-airspace-fighter
author:        surt
social_media:  # Full urls (leave blank to disable)
  website:     http://opengameart.org/users/surt
  twitter:     
  facebook:    
  reddit:      # Subreddit
  tumblr:      
  bitbucket:   
  github:      
  gitlab:      
  deviantart:  
  google-plus:
  vimeo:       
  youtube:     
  soundcloud:  
images:
  thumbnail:   thumbnail.png # 315x250
  banner:      # MIN: 640x360, MAX: any 16:9 ratio image above 640x360 works
  screenshots:
    - fighter.png
tags:
  - Examples
---

An example how a 3D model asset page would look like. _(I personally have zero knowledge on how to make 3D models) // Saucy_
