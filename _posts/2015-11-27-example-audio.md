---
layout:        post
title:         'Example: Audio'
date:          2015-11-27 01:49:24
folder:        /example/audio # Name of your assets folder
homepage:      http://opengameart.org/content/battle-theme-a
preview:       battleThemeA.mp3
download:      battleThemeA.mp3
license:       CC0
license_notes: cynicmusic.com pixelsphere.org
license_url:   http://opengameart.org/content/battle-theme-a
author:        cynicmusic
social_media:  # Full urls (leave blank to disable)
  website:     http://cynicmusic.com/
  twitter:     
  facebook:    
  reddit:      # Subreddit
  tumblr:      
  bitbucket:   
  github:      
  gitlab:      
  deviantart:  
  google-plus:
  vimeo:       
  youtube:     
  soundcloud:  
images:
  thumbnail:   thumbnail.png # 315x250
  banner:      # MIN: 640x360, MAX: any 16:9 ratio image above 640x360 works
  screenshots:
tags:
  - Examples
---

An example how an audio asset page would look like. _(I personally have zero knowledge on how to make music) // Saucy_
