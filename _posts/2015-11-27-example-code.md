---
published:     false
layout:        post
title:         'Example: Code'
date:          2015-11-27 03:49:31
folder:        /example/code # Name of your assets folder
homepage:      https://gitlab.com/snippets/33270
preview:       https://gitlab.com/snippets/33270/raw
download:      https://gitlab.com/snippets/33270/download
license:       MIT License
license_notes:
license_url:   LICENSE.md
author:        Saucy
social_media:  # Full urls (leave blank to disable)
  website:     http://saucy.se
  twitter:     
  facebook:    
  reddit:      # Subreddit
  tumblr:      
  bitbucket:   
  github:      
  gitlab:      
  deviantart:  
  google-plus:
  vimeo:       
  youtube:     
  soundcloud:  
images:
  thumbnail:   thumbnail.png # 315x250
  banner:      # MIN: 640x360, MAX: any 16:9 ratio image above 640x360 works
  screenshots:
tags:
  - Examples
---

An example how a code asset page would look like. _// Saucy_
