---
layout:        post
title:         'Example: Texture'
date:          2015-11-27 00:37:04
folder:        /example/texture # Name of your assets folder
homepage:      http://opengameart.org/content/free-textures-pack-64
preview:       preview.jpg
download:      texture.zip
license:       CC0
license_notes: |
  Please, don't be an ass, don't just resell it. ><

  Please, give me a link of the result at nobiax.deviantart.com or my other account on OpenGameArt.com (yughues) or ShareCG.com (yughues)
  ( watch my journal on deviantart to find them ) ;)

  Or on Youtube.

  For any commissions details are on my DeviantArt account.

  - [yughues @ opengameart](http://opengameart.org/users/yughues)
  - [yughues @ sharecg](http://www.sharecg.com/yughues)
license_url:   LICENSE.md
author:        Nobiax/yughues
social_media:  # Full urls (leave blank to disable)
  website:     
  twitter:     
  facebook:    
  reddit:      # Subreddit
  tumblr:      
  bitbucket:   
  github:      
  gitlab:      
  deviantart:  http://nobiax.deviantart.com/
  google-plus:
  vimeo:       
  youtube:     
  soundcloud:  
images:
  thumbnail:   thumbnail.png # 315x250
  banner:      preview.jpg # MIN: 640x360, MAX: any 16:9 ratio image above 640x360 works
  screenshots:
    - diffuse.png
    - normal.png
tags:
  - Examples
---

An example how a texture asset page would look like. _(I personally have zero knowledge on how to make textures) // Saucy_
