# README
Starcraft 2: Wings of Liberty wallpapers, includes help about the tech-trees and unit counters for all races. [Original thread](http://eu.battle.net/sc2/en/forum/topic/441825881#1) on Battle.net.

Source .psd files are included in the .zip file!

Starcraft 2 [font on dafont.com](http://www.dafont.com/starcraft.font)

_Updated 2015-11-26: Fix both .psd files. Reduced their file size (was over 30mb), layer & groups structure (easier to find the different races)._
